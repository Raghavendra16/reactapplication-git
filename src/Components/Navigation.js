import React, { Component } from "react";
import { NavLink, Link } from "react-router-dom";
import { Nav, Navbar } from "react-bootstrap";
import { NavbarBrand } from "reactstrap";

export class Navigation extends Component {
  render() {
    return (
      <Navbar bg="dark" expand="lg" className="mr-auto">
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <NavbarBrand className="mr-auto">
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav>
              <NavLink className="d-inline p-2 bg-dark text-white" to="/">
                Home
              </NavLink>
              <NavLink
                className="d-inline p-2 bg-dark text-white"
                to="/registration"
              >
                Registration
              </NavLink>
              <NavLink
                className="d-inline p-2 bg-dark text-white"
                to="/courses"
              >
                Courses
              </NavLink>
              <NavLink
                className="d-inline p-2 bg-dark text-white"
                to="/students"
              >
                Admin
              </NavLink>
              <NavLink className="d-inline p-2 bg-dark text-white" to="/menu">
                Menu
              </NavLink>
            </Nav>
          </Navbar.Collapse>
        </NavbarBrand>
      </Navbar>
    );
  }
}

export default Navigation;
