import React, { Component } from "react";
import { Button } from "react-bootstrap";
export class Home extends Component {
  constructor(props) {
    super(props);
    this.state = { date: new Date(), welcomealert: false };
  }
  componentDidMount() {
    this.timerID = setInterval(() => this.tick(), 1000);
  }

  componentWillUnmount() {
    clearInterval(this.timerID);
  }

  tick = () => {
    this.setState({
      date: new Date(),
    });
  };
  render() {
    return (
      <div className="mt-5 d-flex justify-content-left">
        <h2>It is {this.state.date.toLocaleTimeString()}.</h2>
      </div>
    );
  }
}

export default Home;
