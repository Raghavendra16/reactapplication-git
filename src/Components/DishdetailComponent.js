import React, { Component } from "react";
import {
  Card,
  CardImg,
  CardImgOverlay,
  CardText,
  CardBody,
  CardTitle,
} from "reactstrap";
import { comments } from "../shared/comments";

import { createPortal } from "react-dom";

export class DishDetail extends Component {
  constructor(props) {
    super(props);
  }

  FilterComments() {
    return comments.filter((com) => com.dishId === this.props.details.id);
  }
  //#region Render
  render() {
    const LoadComments = this.FilterComments().map((com) => {
      return (
        <div className="col-12 col-md-10 m-1">
          <ul className="list-unstyled" key={com.id}>
            <li>
              <div>{com.comment}</div>
              <div>{`-- ${com.author} , ${new Intl.DateTimeFormat("en-US", {
                year: "numeric",
                month: "short",
                day: "2-digit",
              }).format(new Date(Date.parse(com.date)))}`}</div>
            </li>
          </ul>
        </div>
      );
    });

    return (
      <div className="row">
        <div className="col-12 col-md-5 m-1">
          <Card key={this.props.details.id}>
            <CardImg
              src={this.props.details.image}
              alt={this.props.details.name}
            />
            <CardBody>
              <CardTitle>{this.props.details.name}</CardTitle>
              <CardText>{this.props.details.description}</CardText>
            </CardBody>
          </Card>
        </div>
        <div className="col-12 col-md-5 m-1">
          <CardTitle>Comments</CardTitle>
          <div>{LoadComments}</div>
        </div>
      </div>
    );
  }
  //#endregion
}
