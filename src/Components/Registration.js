import React, { Component } from "react";
import { Courses } from "./Courses";

export class Registration extends Component {
  constructor(props) {
    super(props);
  }
  AddStudent = (event) => {
    event.preventDefault();
  };
  render() {
    return (
      <div>
        <form onSubmit={this.AddStudent}>
          <h2>Student Registration</h2>
          <div>
            <label>First Name</label>
            <input type="text" name="firstName" placeholder="First Name" />
          </div>
          <div>
            <label>Last Name</label>
            <input type="text" name="lasttName" placeholder="Last Name" />
          </div>
          <div>
            <label>Email Id</label>
            <input type="text" name="emailId" placeholder="Email Id" />
          </div>
          <div>
            <input type="submit" name="Submit" value="Submit" />
          </div>
        </form>
      </div>
    );
  }
}
export default Registration;
