import React, { Component } from "react";
import { Table, Button } from "react-bootstrap";

export class Courses extends Component {
  constructor(props) {
    super(props);
    this.state = { courses: [] };
  }
  componentDidUpdate() {}
  componentDidMount() {
    this.getCourses();
  }
  getCourses = () => {
    fetch("http://localhost:62665/api/values/Get")
      .then((response) => response.json())
      .then(
        (response) => {
          this.setState({
            courses: response,
          });
        },
        (error) => {
          alert("Could not connect at this moment");
        }
      );
  };
  render() {
    return (
      <div>
        <Table className="mt-4" striped bordered hover size="sm">
          <thead>
            <tr>
              <th>CourseId</th>
              <th>CourseName</th>
            </tr>
          </thead>
          <tbody>
            {this.state.courses.map((courses) => (
              <tr key={courses.CourseId}>
                <td>{courses.CourseId}</td>
                <td>{courses.CourseName}</td>
              </tr>
            ))}
          </tbody>
        </Table>
      </div>
    );
  }
}

export default Courses;
