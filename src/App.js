import React, { Component } from "react";
import logo from "./logo.svg";
import "./App.css";
import { dishes } from "./shared/dishes";
import { comments } from "./shared/comments";
import Button from "react-bootstrap/Button";
import { Home } from "./Components/Home";
import { Registration } from "./Components/Registration";
import { Courses } from "./Components/Courses";
import { Students } from "./Components/Students";
import Menu from "./Components/Menu";
import { BrowserRouter, Route, Switch, NavLink } from "react-router-dom";
import { Navigation } from "./Components/Navigation";

class App extends Component {
  constructor(props) {
    super(props);

    this.state = { dishes: dishes, comments: comments };
  }
  render() {
    return (
      <BrowserRouter>
        <div className="container">
          <Navigation></Navigation>
          <Switch>
            <Route path="/" component={Home} exact />
            <Route path="/registration" component={Registration} />
            <Route path="/courses" component={Courses} />
            <Route path="/students" component={Students} />
            {/* <Route path="/menu" component={Menu} /> */}
            <Menu dishes={this.state.dishes} comments={this.state.comments} />
          </Switch>
        </div>
      </BrowserRouter>
    );
  }
}
export default App;
